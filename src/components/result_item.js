import React from "react";
import { withRouter } from "react-router-dom";

const ResultItem = ({
  show: {
    image: { medium },
    name
  },
  history
}) => {

  return (
    <div className="result_item">
      <img src={medium} alt={"result_pic"} onClick={() => history.push(`/show/${name}`)} />
    </div>
  );
};

export default withRouter(ResultItem);
