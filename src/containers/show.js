import React from "react";
import { withRouter } from 'react-router-dom'
import { useRecoilValue } from "recoil";
import ReactHtmlParser from "react-html-parser";

import { singleShowState } from "../utils/state";

const Show = ({history, match:{params:{showName}} }) => {

const show = useRecoilValue(singleShowState(showName));

  return (
    <div>
      <div className="topbar">
        <button className="back_button" onClick={() => history.push('/')}>Search again</button>
      </div>
    <div className="show_container">
      <div className="show_container_left">
        <img src={show.image.medium} alt={"default"} />
      </div>
      <div className="show_container_right">
        <span>
          <b>Title : </b>
          {show.name}
        </span>
        <span>
          <b>Language : </b>
          {show.language}
        </span>
        <span>
          <b>Type : </b>
          {show.type}
        </span>
        <span>
          <b>Genre : </b>
          {show.genres.join(" , ")}
        </span>
        <span>
          <b>Country : </b>
          {show.network ? show.network.country.name : <b className='red'>UNKNOW</b>}
        </span>
        <span>
          <b>Rating average : </b>
          {show.rating.average ? show.rating.average : <b className='red'>UNKNOW</b>}
        </span>
        <span>
          <b>Status : </b>
          {show.status}
        </span>
        <span>
          <b>Summary : </b>
          {ReactHtmlParser(show.summary)}
        </span>
      </div>
    </div>
    </div>
  );
};

export default withRouter(Show);
