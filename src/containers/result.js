import React from "react";
import { useRecoilState } from "recoil";
import { v4 as uuidv4 } from "uuid";

import { resultState } from "../utils/state";

import ResultItem from "../components/result_item";

const Result = ({history}) => {
  const [result, setResult] = useRecoilState(resultState);
  React.useEffect(()=> {
    return () => setResult([])
  },[setResult])
  return (
    <>
      <div className="topbar">
        <button className="back_button" onClick={() => history.push('/')}>Go back</button>
      </div>
      <div className="result_container">
        {result.map(ele => (
          <ResultItem key={uuidv4()} {...ele} />
        ))}
      </div>
    </>
  );
};

export default Result;
