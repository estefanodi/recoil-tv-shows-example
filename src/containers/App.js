import React from "react";
import { useRecoilValue } from "recoil";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from "react-router-dom";

import { resultState } from "../utils/state";

import Search from "./search";
import Result from "./result";
import Show from "./show";

function App() {
  const result = useRecoilValue(resultState);
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Search} />
        <Route
          exact
          path="/result"
          render={(props) =>
            result.length === 0 ? <Redirect to={"/"} /> : <Result {...props} />
          }
        />
        <Route
          exact
          path="/show/:showName"
          render={(props) => (
            <React.Suspense fallback={<div>Loading...</div>}>
              <Show {...props} />
            </React.Suspense>
          )}
        />
      </Switch>
    </Router>
  );
}

export default App;
