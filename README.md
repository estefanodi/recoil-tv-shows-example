# Recoil tv shows mini app

Technologies :

1. React

2. Recoil

3. React Router 5

### How to run it: 

```
git clone https://estefanodi@bitbucket.org/estefanodi/recoil-tv-shows-example.git 
cd recoil-tv-shows-example
npm install
npm start
```

### Description:

Mini app to search tv shows from an external api using Recoil as a state manager.

### You can try it at :

test-recoil.surge.sh
